#!/usr/bin/env python3

import numpy as np
import pudb

def loadData():
    R = np.zeros((1, 3, 3))
    t = np.zeros((1, 3, 1))

    for i in range(1, 4):
        for j in range(i+1, 5):
            file_path = 'data/parameters_' + str(i) + 't' + str(j) + '.txt'
            f = open(file_path, 'r')
            num_line = 1
            list_R = []
            R_ = np.zeros((3, 3))
            t_ = np.zeros((3, 1))

            while True:
                line = f.readline()
                num_line += 1
                if 19 < num_line < 23:
                    list_R_ = []
                    str_R = line.split(' ')
                    for k in range(3): list_R_.append(float(str_R[k]))
                    list_R.append([list_R_[0], list_R_[1], list_R_[2]])
                    if num_line == 22: R_ = np.array(list_R)
                elif num_line == 24:
                    list_t = []
                    str_t = line.split(' ')
                    for k in range(3): 
                        list_t.append(float(str_t[k]))
                    t_ = np.array(list_t)
                    t_ = np.expand_dims(t_, axis=1)
                if not line: break

            R = np.concatenate((R, np.expand_dims(R_, axis=0)), axis=0)
            t = np.concatenate((t, np.expand_dims(t_, axis=0)), axis=0)
            f.close()
    return R[1:], t[1:]

def homogMatfromRotAndTrans(R, tvec):
    """homogMatfromRotAndTrans.

    :param R:
    :param tvec:
    """
    T = np.eye(4)
    T[:3, :3] = R
    tvec = np.squeeze(tvec, axis=1)
    T[:3, 3] = tvec
    return T

def homogeneousInverse(T):
    invT = np.eye(4)
    R = T[:3, :3]
    t = T[:3, 3]
    t = np.expand_dims(t, axis=1)
    invT[:3, :3] = R.T
    invT[:3, 3] = np.squeeze(-R.T.dot(t), axis=1)
    return invT

def translationError(T1, T2):
    return np.linalg.norm(T1-T2)

def orientationErrorByRotation(R1, R2):
    """orientationErrorByRotation.

    :param R1: Numpy array (3, 3)
    :param R2: Numpy array (3, 3) 
    :return Error: Scalar
    """
    rel_R = np.dot(np.linalg.inv(R1), R2)
    ori_error = 0.5 * np.array([rel_R[2, 1] - rel_R[1, 2], rel_R[0, 2] - rel_R[2, 0], rel_R[1, 0] - rel_R[0, 1]] ) 
    return np.linalg.norm(ori_error) * 180/3.14

def orientationErrorByRod(R1, R2):
    rvec1 = np.empty((3,1))
    rvec2 = np.empty((3,1))
    cv2.Rodrigues(R1, rvec1)
    cv2.Rodrigues(R2, rvec2)
    ang1, rvec1 = rodVectorToAngle(rvec1)
    ang2, rvec2 = rodVectorToAngle(rvec2)
    diff_ang = np.arccos(np.dot(rvec1.reshape(1, 3), rvec2) / (np.linalg.norm(rvec1) * np.linalg.norm(rvec2)))
    return diff_ang


if __name__ == "__main__":
    R, t = loadData()
    R21, R31, R41 = R[:3]
    R32, R42 = R[3:5]
    R43 = R[5]

    t21, t31, t41 = t[:3]
    t32, t42 = t[3:5]
    t43 = t[5]

    T21 = homogMatfromRotAndTrans(R21, t21)
    T31 = homogMatfromRotAndTrans(R31, t31)
    T41 = homogMatfromRotAndTrans(R41, t41)
    T32 = homogMatfromRotAndTrans(R32, t32)
    T42 = homogMatfromRotAndTrans(R42, t42)
    T43 = homogMatfromRotAndTrans(R43, t43)

    # Inverse
    T12 = homogeneousInverse(T21)
    T13 = homogeneousInverse(T31)
    T14 = homogeneousInverse(T41)
    T23 = homogeneousInverse(T32)
    T24 = homogeneousInverse(T42)
    T34 = homogeneousInverse(T43)
    
    T13_ = T12.dot(T23)
    T14_ = T12.dot(T23).dot(T34)
    T14__ = T12.dot(T24)
    T14___ = T13.dot(T34)
    T24_ = T23.dot(T34)

    e_t13 = translationError(T13[:3, 3], T13_[:3, 3])
    e_t14_1 = translationError(T14[:3, 3], T14_[:3, 3])
    e_t14_2 = translationError(T14[:3, 3], T14__[:3, 3])
    e_t14_3 = translationError(T14[:3, 3], T14___[:3, 3])
    e_t24 = translationError(T24[:3, 3], T24_[:3, 3])

    e_R13 = orientationErrorByRotation(T13[:3, :3], T13_[:3, :3])
    e_R14_1 = orientationErrorByRotation(T14[:3, :3], T14_[:3, :3])
    e_R14_2 = orientationErrorByRotation(T14[:3, :3], T14__[:3, :3])
    e_R14_3 = orientationErrorByRotation(T14[:3, :3], T14___[:3, :3])
    e_R24 = orientationErrorByRotation(T24[:3, :3], T24_[:3, :3])

    print('T12', np.linalg.norm(T12[:3, 3]))

    print('Translation error[mm]')
    print('1->3:        ', e_t13)
    print('1->2->3->4:  ', e_t14_1)
    print('1->2->4:     ', e_t14_2)
    print('1->3->4:     ', e_t14_3)
    print('2->3->4:     ', e_t24)
    print('---')
#    print('Orientation error')
#    print('1,3', e_R13)
#    print('1,4', e_R14_1)
#    print('1,4', e_R14_2)
#    print('1,4', e_R14_3)
#    print('2,4', e_R24)
